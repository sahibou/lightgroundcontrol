package com.sdo.module2.app;

import java.io.File;
import java.io.IOException;
import java.lang.instrument.ClassDefinition;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.sun.jdi.*;
import com.sun.jdi.connect.*;

public class App {

	private static Class debugClass = null;

	private static AttachingConnector _attachingConnector = null;
	private static URLClassLoader _urlClassLoader = null;
	private static VirtualMachine _jvm = null;

	private static String HOST="localhost";
	private static String PORT="18080";
	
	public static void main(String[] args) throws Exception {

		getLc();
		getUrlcl();
		getDebugClass();

		loopRedefineClass();
		// System.out.println(vm);
		// System.out.println(vm.canRedefineClasses());

	}

	private static void loopRedefineClass() throws InterruptedException, IOException {

		while (true) {
			ReferenceType _rt = null;
			Map<ReferenceType, byte[]> references = new HashMap<ReferenceType, byte[]>();

			for (ReferenceType rt : _jvm.allClasses()) {
				if (rt.name().toString().equals("com.sdo.module1.app.Message")) {
					System.out.println("FOUND Class reference :" + rt);
					references.put(rt, Files.readAllBytes((new File(
							"C:/dev/_WOSP/crashtest4/crashtest/module1/target/classes/com/sdo/module1/app/Message.class"))
									.toPath()));
					System.out.println("REDEFINING");
					_jvm.redefineClasses(references);
				} else {
					// System.out.println("<"+rt.name().toString()+">");
				}
			}

			Thread.sleep(10000);
		}
	}

	//INIT 1
	private static AttachingConnector getLc() throws IOException, IllegalConnectorArgumentsException {
		AttachingConnector lc = connector("com.sun.jdi.SocketAttach");

		Map arguments = lc.defaultArguments();
		((Connector.Argument) arguments.get("hostname")).setValue(HOST);
		((Connector.Argument) arguments.get("port")).setValue(PORT);
		VirtualMachine jvm = lc.attach(arguments);

		System.out.println("GOT jvm !! " + jvm);

		_jvm = jvm;
		_attachingConnector = lc;
		return lc;

	}
	private static AttachingConnector connector(String connectorName) {
		for (AttachingConnector c : Bootstrap.virtualMachineManager().attachingConnectors()) {
			System.out.println(c.name());
			if (connectorName.equals(c.name())) {
				return c;
			}
		}
		return null;
	}

	//INIT 2
	//TODO, has to renex URLCLASSLOADER AFTER REDEFINE.
	private static URLClassLoader getUrlcl() throws MalformedURLException, ClassNotFoundException {
		URL[] urls = new URL[1];
		urls[0] = new URL("file:/C:/dev/_WOSP/crashtest4/crashtest/module1/target/classes/");
		URLClassLoader urlcl = new URLClassLoader(urls);

		_urlClassLoader = urlcl;
		return urlcl;
	}

	private static Class getDebugClass() throws ClassNotFoundException {
		Class<?> debugClass = _urlClassLoader.loadClass("com.sdo.module1.app.Message");
		return debugClass;
	}

}
